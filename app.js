var app = require('http').createServer(handler)
    , io = require('socket.io').listen(app)
    , fs = require('fs')
    , __ = require('underscore')
    , amqp = require('amqplib');

app.listen(3000);

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
            res.writeHead(200);
            res.end(data);
        });
}

io.sockets.on('connection', function (socket) {

    socket.on('user-conected', function (data) {
        socket.email = data.email;
        socket.user_id = data.id;
        var users_online = __.map(io.sockets.sockets, function (s, index) {
            return {id: s.user_id};
        });
        socket.emit("user-online-list", users_online);
        io.sockets.emit('new-user-online', {id: data.id});
    });
    socket.on("disconnect", function () {
        io.sockets.emit('leave-user-online', socket["user_id"]);
    });
});

amqp.connect('amqp://localhost').then(function (conn) {
    process.once('SIGINT', function () {
        conn.close();
    });
    return conn.createChannel().then(function (ch) {

        var ok = ch.assertQueue('miranda', {durable: false});

        ok = ok.then(function (_qok) {
            return ch.consume('miranda', function (msg) {
                msg = JSON.parse(msg.content.toString());
                __.each(io.sockets.sockets, function (value, key) {
                    if (value.user_id == msg.to_user || value.user_id == msg.from_user) {
                        io.sockets.sockets[key].emit("chat", msg);
                    }

                });
            }, {noAck: true});
        });

        return ok.then(function (_consumeOk) {
            console.log(' [*] Waiting for messages. To exit press CTRL+C');
        });
    });
}).then(null, console.warn);